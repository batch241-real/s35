const express = require("express");

const mongoose = require("mongoose");

const app = express();

const port = 3001;

mongoose.connect("mongodb+srv://admin123:admin123@cluster0.ug17qel.mongodb.net/s35?retryWrites=true&w=majority",{
	useNewUrlParser: true,
	useUnifiedTopology: true
});


// connect MongoDB
let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"))

db.once("open", () => console.log("Connected to MongoDB Atlas!"));

app.use(express.json());

app.use(express.urlencoded({extended: true}));


// schema
const signupSchema = new mongoose.Schema({
	username: String,
	password: String,
});


const Signup = mongoose.model("Signup", signupSchema);

app.post('/signup', (req, res) => {
	Signup.findOne({username: req.body.username}, (err, result) => {
		if(result !== null && result.username == req.body.username){
			return res.send("User is already registered!");
		} else{
			let newSignup = new Signup({
				username: req.body.username,
				password: req.body.password
			});

			newSignup.save((saveErr, savedSignup) => {
				if(saveErr){
					return console.error(saveErr);
				} else{
					return res.status(201).send("New user registered!")
				};
			});
		};
	});
});


app.listen(port, () => console.log(`Server running at port ${port}`));